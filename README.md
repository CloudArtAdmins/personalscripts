# PersonalScripts

These are just some scripts I've made (or modified) so I don't need to remember the command nor do all the steps for what I want to do.

## update_git_repos.sh
Don't remember where this originally came from, but I modified this script to pull all repos in a directory.

## GrabGitList.sh
I have a folder full of repositories I like having cloned. 
This pulls the repository URL and puts them into a file for me.

## audioFormatConverter.sh
ffmpeg does not have the ability to recursively search for files to convert. 
Made to prevent the need of going through numberous directories to convert files 1 by 1.  
This should work with extra ffmpeg options, and will split each file conversion into a new thread to make full use of the CPU.
