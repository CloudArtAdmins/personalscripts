#!/bin/sh

# Let the person running the script know what's going on.
echo -e "\n\033[1mPulling in latest changes for all repositories... \033[0m\n"

# In parallel, print repo name in yellow, pull the directory of repo.
# Found by finding .git files, and slicing off the ".git" from found directory.
parallel " 
	echo -e \"\n\033[33m\"{}\"\033[0m\"; 
	git -C {} pull;" \
	::: $(find . -name ".git" | rev | cut -f 2- -d '.' | rev)

# Find all git repositories and update it to the master latest revision
#for i in $(find . -name ".git" | rev| cut -f 2- -d '.' | rev)
#do
    #echo "";
    #echo -e "\033[33m"$i"\033[0m";

    ##pull from directories
    #git -C $i pull;

#done

echo -e "\n\033[32mComplete!\033[0m\n"
